var circularProgressBar =
{
	init:function(canvas){
		this.context=canvas.getContext('2d');
		this.centerX=canvas.width / 2;
	    this.centerY=canvas.height / 2;
	    this.radius=(Math.min(canvas.height,canvas.width)-1)/2;
        this.full=this.radius*2;
	},

	draw:function(percentage) {
		amount=this.full*percentage/100;
		this.context.save();
		this.context.beginPath();
		this.context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI, false);
		this.context.clip(); // Make a clipping region out of this path
		// instead of filling the arc, we fill a variable-sized rectangle
		// that is clipped to the arc
		this.context.fillStyle = '#13a8a4';
		// We want the rectangle to get progressively taller starting from the bottom
		// There are two ways to do this:
		// 1. Change the Y value and height every time
		// 2. Using a negative height
		// I'm lazy, so we're going with 2
		this.context.fillRect(this.centerX - this.radius, this.centerY + this.radius, this.radius * 2, -amount);
		this.context.restore(); // reset clipping region

		this.context.beginPath();
		this.context.arc(this.centerX, this.centerY, this.radius, 0, 2 * Math.PI, false);
		this.context.lineWidth = 1;
		this.context.strokeStyle = '#000000';
		this.context.stroke();
		
		//retour utilisateur quand 100%
		if(percentage==100)
		{
			this.context.font = "20px Lucida";
			this.context.strokeText("Completed!",this.centerX - 46,this.centerY+6);
		}
	}

}
 //use
/*circularProgressBar.init(document.getElementById('counter'))
circularProgressBar.draw(62)*/