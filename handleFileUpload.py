#!/usr/bin/python
import cgi
import shutil
import os
import cgitb

def save_uploaded_file (form_field, upload_dir):
    cgitb.enable()    


    #The file upload draft standard entertains the possibility of uploading multiple files from one field (using a recursive multipart/* encoding). 
    form = cgi.FieldStorage()
    if not form.has_key(form_field):
        return "form not has key"
    fileitem = form[form_field]

    if not fileitem.file:
        return "fileitem not file"
       
    content=fileitem.value
    if "<script>" in content:
        return "Well tried, sucker !"
        
    outpath = os.path.join(upload_dir, fileitem.filename)

    with open(outpath, 'wb') as fout:
        shutil.copyfileobj(fileitem.file, fout)
        
    return "File uploaded"    
    
if __name__ == '__main__':
    ret = save_uploaded_file("articleToUpload","articles/html/fr/")
    print 'Content-type: text/html\n\n' #les \n\n sont important me semble par rapport au cgi

    print "<!DOCTYPE html>"
    print "<html>"
    print '<link rel="stylesheet" type="text/css" href="style.css"/>'
    print "<body>"
    print ret
    print"</body>"
    print"</html>"
