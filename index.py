#!/usr/bin/python
import os

#upload en html les articles et python va leur ajouter l'en tete avec le style

articleList=[]
for filename in os.listdir("upload"):
    if not os.path.isdir("upload/"+filename) and filename!="index.html":
        articleList.append('        <li class="articleListElement" > <a href="upload/'+filename+'">'+filename.split(".")[0]+'</a> </li>')


print 'Content-type: text/html\n\n' #les \n\n sont important me semble par rapport au cgi

print "<!DOCTYPE html>"
print "<html>"
print '<link rel="stylesheet" type="text/css" href="style.css"/>'
print "<body>"
print '    <div id="header">'
print '        <h1 id="mainTitle">Stuff Uploaded</h1> '
print '    </div>'
print '    <ul id="articleList">'
for elt in articleList:
    print elt
print '    </ul>'
print"</body>"
print "<footer>"
print '   <p class="button"><a href="uploadRepet.html">Upload un truc !</a></p>'
print "</footer>"
print"</html>"
